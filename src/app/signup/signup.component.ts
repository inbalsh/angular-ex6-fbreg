import { Component, OnInit } from '@angular/core';
import {AuthService} from '../auth.service'; // הוספת אימפורט
import {Router} from "@angular/router";
import { AngularFireAuth } from '@angular/fire/auth';
import { auth } from 'firebase/app';

@Component({
  selector: 'signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {

  
  // הוספת משתנים והסוג שלהם
  email: string;
  password: string;
  name: string;
  error = '';  // הוספת תכונה של המחלקה
  output = '';
  required = '';
  require = true;
  flag = false;
  specialChar = ['!','@','#','$','%','^','&','*','(',')','_','-','+','='];
  valid = false;
  validation = "";

 
  register(){
 //   console.log("sign up clicked" + " " + this.email + " " + this.password + " " + this.name)
    this.flag = false;
    this.valid = false;
    this.require = true;


    if (this.name == null || this.password == null || this.email == null) 
    {
      this.required = "this input is required";
      this.require = false;
    }

    if(this.require)
    {
      for (let char of this.specialChar)
      {
        if (this.password.includes(char))
        {
          this.valid = true;
        }
      }
    }
    if (this.valid && this.require)
    {
      this.authService.register(this.email,this.password)   // promise
      .then(value => {//{console.log(value)}
      // then משמע - ברגע שמתרחש אירוע אז.......
      // בסוגריים נשתמש ב arroe function
      // הוליו זה מה שפיירבייס מחזיר לנו
      // כדי לדעת ממה הויליו שהגיע מפיירבייס מורכב, נכתוב קונסול
        this.authService.updateProfile(value.user,this.name); // ליוזר שיצרת תוסיפי את השם הזה
      }).then(value => {
        this.router.navigate(['/welcome'],  { queryParams: { name: this.name } });
        // במידה ויש שגיאה לא ילך לכל הט'נ שכתבנו קודם אלא יבוא ישר לקטצ'..ההבטחה לא התקיימה
      }).catch(err => { // המטרה ללכוד שגיאות ולדווח למשתמש
  // אם ההודעה היא תכונה של המחלקה, ניתן להעבירה לטמפלייט
        this.flag = true;
        this.error = err.code;
        this.output = err.message;
        console.log("error" + this.error);
        console.log("output" + this.output);
    })
  }
  else
  {
    this.validation = "Password must contain special characters";
  }
  //console.log("register " + this.name + ' ' + this.email + ' ' + this.password);

}

constructor(private authService:AuthService, private router:Router, public afAuth: AngularFireAuth)
 { }

  ngOnInit() {
  }

  onLoginGoogle()
  {
    this.afAuth.auth.signInWithPopup(new auth.GoogleAuthProvider());
    this.router.navigate(['/welcome']);
  }

  onLoginFacebook()
  {
    this.afAuth.auth.signInWithPopup(new auth.FacebookAuthProvider());
    this.router.navigate(['/welcome']);
  }


}
