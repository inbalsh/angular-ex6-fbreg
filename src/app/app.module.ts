import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule} from '@angular/forms'; // בשביל טופס ההרשמה
import {BrowserAnimationsModule} from '@angular/platform-browser/animations'; // בשביל ההרשמה

//firebase modules
import { AngularFireModule } from '@angular/fire';
import { AngularFireDatabaseModule } from '@angular/fire/database';
import { AngularFireAuthModule } from '@angular/fire/auth';

//material angular
import {MatInputModule} from '@angular/material/input'; // בשביל ההרשמה
import { MatCardModule } from '@angular/material/card';
import { MatButtonModule } from '@angular/material/button';

//import { AngularFontAwesomeModule } from 'angular-font-awesome';

import { AppComponent } from './app.component';
import { SignupComponent } from './signup/signup.component';
import { WelcomeComponent } from './welcome/welcome.component';
import { Routes, RouterModule } from '@angular/router'; // פקודה להבאת הנתיבים

import {environment} from '../environments/environment'; // הוספת אימפורט

@NgModule({
  declarations: [
    AppComponent,
    SignupComponent,
    WelcomeComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AngularFireModule.initializeApp(environment.firebase), // firebase
    AngularFireDatabaseModule, //fb
    AngularFireAuthModule, // fb
  //  AngularFontAwesomeModule,
    MatInputModule, // for registration
    MatCardModule,
    MatButtonModule,
    BrowserAnimationsModule, // for registration
    RouterModule.forRoot([ // כאן נגדיר נתיב, כל נתיב הינו ג'ייסון
    {path:'', component:SignupComponent}, //ברירת מחדל
    //{path:'signup', component:SignupComponent},
    {path:'welcome', component:WelcomeComponent},
    {path:'**', component:SignupComponent} // אם היוזר מכניס קישור לא מוכר, לכאן זה יגיע
    ]),
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
