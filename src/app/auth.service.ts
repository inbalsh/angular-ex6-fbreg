import { Injectable } from '@angular/core';
import {AngularFireAuth} from '@angular/fire/auth';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  register(email:string, password:string) // אפשר לקרוא לפונקציה בשם אחר. אין קשר לפונקצייה סיינאפ השנייה בהרשמה
  {
    return this.fireBaseAuth
              .auth
              .createUserWithEmailAndPassword(email,password);
  }

  updateProfile(user, name:string){
    // השם שיגיע מהסוגריים למעלה ייכנס לתוך דיספלייניימ שזהו שם שמור
    user.updateProfile({displayName:name, photoURL:''}); // אפדייטפרופייל זו פונקציה של יוזר שמעדכנת את הפרופיל של יוזר
  }

  isAuth()
  {
    return this.fireBaseAuth.authState.pipe(map(auth => auth));
  }


  constructor(private fireBaseAuth:AngularFireAuth) //מסוג אנגולרפייראוט'
   {

    }
}